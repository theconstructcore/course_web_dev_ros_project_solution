var app = new Vue({
    el: '#app',
    // storing the state of the page
    data: {
        connected: false,
        ros: null,
        logs: [],
        loading: false,
        rosbridge_address: '',
        port: '9090',
        topicCmdVel: null,
        actionClient: null,
        goal: null,
        action: {
            goal: { position: {x: 0, y: 0, z: 0} },
            feedback: { position: 0, state: 'idle' },
            result: { success: false },
            status: { status: 0, text: '' },
        }
    },
    // helper methods to connect to ROS
    methods: {
        connect: function() {
            this.loading = true
            this.ros = new ROSLIB.Ros({
                url: this.rosbridge_address
            })
            this.ros.on('connection', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Connected!')
                this.connected = true
                this.loading = false

                // robot 3D model viewer
                this.setup3DViewer()
                // map
                this.setupMap()
                // camera
                this.setupCamera()
            })
            this.ros.on('error', (error) => {
                this.logs.unshift((new Date()).toTimeString() + ` - Error: Not possible to connect`)
            })
            this.ros.on('close', () => {
                this.logs.unshift((new Date()).toTimeString() + ' - Disconnected!')
                this.connected = false
                this.loading = false

                // robot 3D model viewer
                this.unset3DViewer()
                // map
                this.unsetMap()
                // camera
                this.unsetCamera()
            })
        },
        disconnect: function() {
            this.ros.close()
        },
        setupTopicCmdVel: function() {
            if (this.topicCmdVel == null) {
                this.topicCmdVel = new ROSLIB.Topic({
                    ros: this.ros,
                    name: '/cmd_vel',
                    messageType: 'geometry_msgs/Twist'
                })
            }
        },
        commandGoAhead: function() {
            this.setupTopicCmdVel()
            let message = new ROSLIB.Message({
                linear: { x: 0.7, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: 0, },
            })
            this.topicCmdVel.publish(message)
        },
        commandTurnLeft: function() {
            this.setupTopicCmdVel()
            let message = new ROSLIB.Message({
                linear: { x: 0, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: 0.5, },
            })
            this.topicCmdVel.publish(message)
        },
        commandTurnRight: function() {
            this.setupTopicCmdVel()
            let message = new ROSLIB.Message({
                linear: { x: 0, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: -0.5, },
            })
            this.topicCmdVel.publish(message)
        },
        commandStop: function() {
            this.setupTopicCmdVel()
            let message = new ROSLIB.Message({
                linear: { x: 0, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: 0, },
            })
            this.topicCmdVel.publish(message)
        },
        commandBackward: function() {
            this.setupTopicCmdVel()
            let message = new ROSLIB.Message({
                linear: { x: -0.7, y: 0, z: 0, },
                angular: { x: 0, y: 0, z: 0, },
            })
            this.topicCmdVel.publish(message)
        },
        setupActionClient: function() {
            if (this.actionClient == null) {
                this.actionClient = new ROSLIB.ActionClient({
                    ros : this.ros,
                    serverName : '/turtlebot2_action_service_as',
                    actionName : 'course_web_dev_ros/WaypointActionAction'
                })
            }
        },
        commandWaypoint: function(x, y) {
            this.setupActionClient()

            if (this.goal != null) {
                this.goal.cancel()
            }

            this.action.goal.position = { x: x, y: y, z: 0, }

            this.goal = new ROSLIB.Goal({
                actionClient : this.actionClient,
                goalMessage: {
                    ...this.action.goal
                }
            })

            this.goal.on('status', (status) => {
                this.action.status = status
            })

            this.goal.on('feedback', (feedback) => {
                this.action.feedback = feedback
            })

            this.goal.on('result', (result) => {
                this.action.result = result
            })

            this.goal.send()
        },
        setupCamera() {
            let without_wss = this.rosbridge_address.split('wss://')[1]
            let domain = without_wss.split('/')[0] + '/' + without_wss.split('/')[1]
            let host = domain + '/cameras'
            let viewer = new MJPEGCANVAS.Viewer({
                divID: 'divCamera',
                host: host,
                width: 320,
                refreshRate: 30,
                quality: 50,
                height: 240,
                topic: '/camera/rgb/image_raw',
                ssl: true,
            })
        },
        unsetCamera() {
            document.getElementById('divCamera').innerHTML = ''
        },
        setupMap() {
            this.mapViewer = new ROS2D.Viewer({
                divID: 'map',
                width: 400,
                height: 300
            })

            // Setup the map client.
            this.mapGridClient = new ROS2D.OccupancyGridClient({
                ros: this.ros,
                rootObject: this.mapViewer.scene,
                continuous: true,
            })
            // Scale the canvas to fit to the map
            this.mapGridClient.on('change', () => {
                this.mapViewer.scaleToDimensions(this.mapGridClient.currentGrid.width, this.mapGridClient.currentGrid.height);
                this.mapViewer.shift(this.mapGridClient.currentGrid.pose.position.x, this.mapGridClient.currentGrid.pose.position.y)
            })
        },
        unsetMap() {
            document.getElementById('map').innerHTML = ''
        },
        setup3DViewer() {
            this.viewer = new ROS3D.Viewer({
                background: '#cccccc',
                divID: 'div3DViewer',
                width: 400,
                height: 300,
                antialias: true,
                fixedFrame: 'odom'
            })

            // Add a grid.
            this.viewer.addObject(new ROS3D.Grid({
                color:'#888888',
                cellSize: 0.5,
                num_cells: 20
            }))

            // Setup a client to listen to TFs.
            this.tfClient = new ROSLIB.TFClient({
                ros: this.ros,
                angularThres: 0.01,
                transThres: 0.01,
                rate: 10.0
            })

            // Setup the URDF client.
            this.urdfClient = new ROS3D.UrdfClient({
                ros: this.ros,
                param: 'robot_description',
                tfClient: this.tfClient,
                // We use "path: location.origin + location.pathname"
                // instead of "path: window.location.href" to remove query params,
                // otherwise the assets fail to load
                path: location.origin + location.pathname,
                rootObject: this.viewer.scene,
                loader: ROS3D.COLLADA_LOADER_2
            })
        },
        unset3DViewer() {
            document.getElementById('div3DViewer').innerHTML = ''
        },
    },
    mounted() {
    },
})
